package com.athomas.restwsnonjpa.entities;

import java.math.BigInteger;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TEAM_MEMBERSHIP")
public class TeamMembership {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "subteam_id")
	private Integer subTeamId;

	@Column(name = "user_id")
	private UUID userId;

	@Column(name = "deleted")
	private boolean deleted;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSubTeamId() {
		return subTeamId;
	}

	public void setSubTeamId(Integer subTeamId) {
		this.subTeamId = subTeamId;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "TeamMembership [id=" + id + ", subTeamId=" + subTeamId + ", userId=" + userId + ", deleted=" + deleted
				+ "]";
	}

}
