package com.athomas.restwsnonjpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TEAM")
public class Team {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "parent_team")
	private Integer parentTeam;

	@Column(name = "deleted")
	boolean deleted;

	public Team() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentTeam() {
		return parentTeam;
	}

	public void setParentTeam(Integer parentTeam) {
		this.parentTeam = parentTeam;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	@Override
	public String toString() {
		return "Team [id=" + id + ", name=" + name + ", parentTeam=" + parentTeam + ", deleted=" + deleted + "]";
	}

}
