package com.athomas.restwsnonjpa.service;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public interface UsersService {
	@CrossOrigin(origins = "*", allowedHeaders = "*", methods=RequestMethod.GET)
	@RequestMapping(value = "userservice/users", method = RequestMethod.GET, produces = "application/json")
	public String getUsers();

	@CrossOrigin(origins = "*", allowedHeaders = "*",methods=RequestMethod.GET )
	@RequestMapping(value = "userservice/user", method = RequestMethod.GET, produces = "application/json")
	public String getUser(@RequestParam String id);

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods=RequestMethod.POST)
	@RequestMapping(value = "userservice/createuser", method = RequestMethod.POST, produces = "application/json")
	public String createUser(@RequestParam String firstName, @RequestParam String lastName, @RequestParam String email,
			@RequestParam String phone, @RequestParam String location, @RequestParam int company_name,
			@RequestParam String position);

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods=RequestMethod.PUT)
	@RequestMapping(value = "userservice/updateuser", method = RequestMethod.PUT, produces = "application/json")
	public void updateUser(@RequestParam(required = true) String id, @RequestParam(required = false) String firstName,
			@RequestParam(required = false) String lastName, @RequestParam(required = false) String email,
			@RequestParam(required = false) String phone, @RequestParam(required = false) String location,
			@RequestParam(required = false) Integer company_name, @RequestParam(required = false) String position);

	@CrossOrigin(origins = "*", allowedHeaders = "*", methods=RequestMethod.DELETE)
	@RequestMapping(value = "userservice/deleteuser", method = RequestMethod.DELETE, produces = "application/json")
	public String deleteUser(@RequestParam String id);
}
