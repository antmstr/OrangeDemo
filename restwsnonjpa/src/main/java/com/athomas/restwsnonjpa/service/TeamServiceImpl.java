package com.athomas.restwsnonjpa.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.athomas.restwsnonjpa.DAO.TeamDAO;
import com.athomas.restwsnonjpa.entities.Team;

@Transactional
@Service
public class TeamServiceImpl implements TeamService {
	
	@Autowired
	TeamDAO teamDAO;
	
	@Override
	public List<Team> getTeams() {
		return teamDAO.getTeams();
	}

}
