package com.athomas.restwsnonjpa.service;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.athomas.restwsnonjpa.DAO.CompanyDAO;
import com.athomas.restwsnonjpa.entities.Company;
import com.athomas.restwsnonjpa.entities.Team;

@Service
public class CompanyServiceImpl implements CompanyService {

	@Autowired
	CompanyDAO companyDAO;
	
	@Transactional
	@Override
	public List<Company> getCompanies() {
		return companyDAO.getCompanies();
	}	

}
