package com.athomas.restwsnonjpa.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import org.springframework.stereotype.Service;

import com.athomas.restwsnonjpa.model.Patient;

@Service
public class PatientsServiceImpl implements PatientsService {

	private HashMap<Long, Patient> patients = new HashMap<>();
	private long currentId = 123;

	PatientsServiceImpl() {
		init();
	}

	private void init() {
		Patient patient = new Patient();
		patient.setId(currentId);
		patient.setName("Donna Born");
		patients.put(new Long(patient.getId()), patient);
	}

	@Override
	public List<Patient> getPatients() {
		System.out.println("get running");
		return new ArrayList<Patient>(patients.values());		
	}

	//@Override
	public Patient getPatient(Long id) {
		return patients.get(id);
	}

	//@Override
	public Patient createPatient(Long id, String name) {
		System.out.println("running!!");
		Patient patient = new Patient();
		patient.setId(id);
		patient.setName(name);
		patients.put(new Long(patient.getId()), patient);
		return patient;
	}
}
