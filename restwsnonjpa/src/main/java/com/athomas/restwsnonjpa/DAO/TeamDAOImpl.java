package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.entities.Company;
import com.athomas.restwsnonjpa.entities.Team;

@Repository
public class TeamDAOImpl implements TeamDAO {
	
	@Autowired
	EntityManagerFactory em;
	
	private SessionFactory factory;
	
	private void someService() {
		if (em.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("factory is not a hibernate factory");
		}
		factory = em.unwrap(SessionFactory.class);
	}
	
	@Override
	public List<Team> getTeams() {		
		someService();
		Session session = factory.getCurrentSession();
		List<Team> teams = session.createQuery("from Team").list();		
		return teams;
	}

}
