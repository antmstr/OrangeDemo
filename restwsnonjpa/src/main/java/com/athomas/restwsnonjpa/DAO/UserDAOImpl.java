package com.athomas.restwsnonjpa.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.transaction.Transactional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.athomas.restwsnonjpa.entities.User;

@Repository
public class UserDAOImpl implements UserDAO {
	@Autowired
	EntityManagerFactory emf;
	EntityManager em;
	private SessionFactory factory;

	private void someService() {
		if (emf.unwrap(SessionFactory.class) == null) {
			throw new NullPointerException("factory is not a hibernate factory");
		}
		factory = emf.unwrap(SessionFactory.class);
		em = emf.createEntityManager();
	}

	@Override
	public List<User> getUsers() {
		someService();
		Session session = factory.getCurrentSession();
		List<User> returns = new <User>ArrayList();
		List<User> userall = session.createQuery("from User").list();
		for (User user : userall) {
			if (!user.isDeleted()) {
				returns.add(user);
			}
		}

		return returns;
	}

	@Override
	public User getUser(String userid) {
		someService();
		System.out.println("userid is " + userid);
		User user = em.createQuery("from User WHERE id = '" + userid + "'", User.class).getSingleResult();
		System.out.println("User is " + user);

		if (user == null) {
			return null;
		}
		if (user.isDeleted()) {
			return null;
		}

		return user;
	}

	public User createUser(String firstName, String lastName, String email, String phone, String location,
			int company_name, String position) {
		someService();
		Session session = factory.getCurrentSession();
		session.beginTransaction();
		User user = new User();
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setEmail(email);
		user.setPhone(phone);
		user.setLocation(location);
		user.setCompany_name(company_name);
		user.setPosition(position);
		user.setDeleted(false);
		session.save(user);
		session.getTransaction().commit();
		return user;
	}

	@Override
	public void updateUser(String id, String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position) {
		System.out.println("in here with DAO!!");
		someService();
		Session session = factory.getCurrentSession();
		session.getTransaction().begin();
		User user = (User) session.createQuery("from User where id in ('" + id + "')").uniqueResult();
		boolean addedSet = false;
		String uquery = "update User ";
		if ((firstName != null) && (!firstName.isEmpty())) {
			uquery += " set first_name='" + firstName + "'";
			addedSet = true;
		}
		if ((lastName != null) && (!lastName.isEmpty())) {
			if (addedSet)
				uquery += ",last_name='" + lastName + "'";
			else {
				uquery += " set last_name='" + lastName + "'";
				addedSet = true;
			}
		}
		if ((email != null) && (!email.isEmpty())) {
			if (addedSet)
				uquery += ",email='" + email + "'";
			else {
				uquery += " set email='" + email + "'";
				addedSet = true;
			}
		}
		if ((phone != null) && (!phone.isEmpty())) {
			if (addedSet)
				uquery += ",phone='" + phone + "'";
			else {
				uquery += " set phone='" + phone + "'";
				addedSet = true;
			}
		}
		if ((location != null) && (!location.isEmpty())) {
			if (addedSet)
				uquery += ",location='" + location + "'";
			else {
				uquery += " set location='" + location + "'";
				addedSet = true;
			}
		}
		if ((company_name != null)) {
			if (addedSet)
				uquery += ",company_name=" + company_name;
			else {
				uquery += " set company_name=" + company_name;
				addedSet = true;
			}
		}
		if ((position != null) && (!position.isEmpty())) {
			if (addedSet)
				uquery += ",position='" + position + "'";
			else {
				uquery += " set position='" + position + "'";
				addedSet = true;
			}
		}
		uquery += " where id in ('" + user.getId() + "')";
		System.out.println("uquery is " + uquery);
		Query query = session.createQuery(uquery);
		query.executeUpdate();
		session.getTransaction().commit();

	}

	@Override
	public User deleteUser(String id) {
		someService();
		Session session = factory.getCurrentSession();
		session.getTransaction().begin();
		User user = (User) session.createQuery("from User where id in ('" + id + "')").uniqueResult();
		String uquery = "update User set deleted = " + true + " where id in ('" + user.getId() + "')";
		Query query = session.createQuery(uquery);
		query.executeUpdate();
		session.getTransaction().commit();
		return getUser(id);
	}

	

}
