package com.athomas.restwsnonjpa.DAO;

import java.util.List;
import com.athomas.restwsnonjpa.entities.Company;

public interface CompanyDAO {

	public List<Company> getCompanies();

}
