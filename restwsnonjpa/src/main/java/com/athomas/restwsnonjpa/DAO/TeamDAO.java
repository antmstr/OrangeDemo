package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import com.athomas.restwsnonjpa.entities.Team;

public interface TeamDAO {
	
	public List<Team> getTeams();
	
}
