package com.athomas.restwsnonjpa.DAO;

import java.util.List;

import com.athomas.restwsnonjpa.entities.User;

public interface UserDAO {

	public List<User> getUsers();

	public User getUser(String id);

	public User createUser(String firstName, String lastName, String email, String phone, String location,
			int company_name, String position);

	public User deleteUser(String id);

	public void updateUser(String id, String firstName, String lastName, String email, String phone, String location,
			Integer company_name, String position);
}
